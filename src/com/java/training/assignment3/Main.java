package com.java.training.assignment3;

public class Main {

    public static void main(String[] args){

        ProcessClass process = new ProcessClass();

            System.out.println("*** Welcome to a simple java program ***");
            System.out.println("---");
            process.divideByTen();
            System.out.println("---");
            process.pickNumberFromList();
            System.out.println("---");
            process.enterCode();
            System.out.println("---");
            System.out.println("*** Program finished ***");
    }
}
