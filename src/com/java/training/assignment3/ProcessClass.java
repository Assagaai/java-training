package com.java.training.assignment3;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class ProcessClass {

    public void divideByTen(){ //Arithmetic exceptions

        int randomNumber = new Random().nextInt(10);

        for(boolean x = true; x;) {
            try {
                Scanner input = new Scanner(System.in);
                System.out.println("Which number can divide "+randomNumber+" ?");
                int number = input.nextInt();
                System.out.println(randomNumber+" divided by " + number + " = " + (randomNumber / number));
                x = false;
            } catch (ArithmeticException ex) {
                System.out.println(randomNumber+" cannot be divided by 0,insert a different value");
            }
        }
    }

    public void pickNumberFromList(){ //ArrayIndexOutOfBoundsException

        for(boolean x = true; x;) {
            try {
                Scanner input = new Scanner(System.in);
                int[] numbers = {1, 2, 3, 4, 5, 6, 7, 9};
                System.out.println("Pick a number from the list: " + Arrays.toString(numbers));
                int pickedNumber = input.nextInt();
                System.out.println("You have picked the number: " + numbers[pickedNumber]);
                x = false;
            } catch (ArrayIndexOutOfBoundsException ex) {
                System.out.println("The position of the values begins from 0 - 8, please choose accordingly");
            }
        }
    }

    public void enterCode(){ //NumberFormatException

        for(boolean x = true; x;) {
            try {
                Scanner input = new Scanner(System.in);
                System.out.println("Please enter any code");
                int code = Integer.parseInt(input.nextLine());
                System.out.println("Your entered code is: " + code);
                x = false;
            } catch (NumberFormatException ex) {
                System.out.println("Please enter a numeric code");
            }
        }
    }
}
