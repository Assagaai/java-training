package com.java.training.assignment1;

public class Building {

    public String floor;
    public String door;
    public String window;
    public String roof;
    public String lights;

    public Building (String floor, String door, String window, String roof, String lights){
        this.floor = floor;
        this.door = door;
        this.window = window;
        this.roof = roof;
        this.lights = lights;
    }

    public void openCloseDoor(){
        System.out.println("opening and closing door");
    }
    public void openCloseWindow(){
        System.out.println("Opening and closing window");
    }
    public void switchLightOnOff(){
        System.out.println("Switching lights on and off");
    }

    public String getFloor() {
        return floor;
    }

    public String getDoor() {
        return door;
    }

    public String getWindow() {
        return window;
    }

    public String getRoof() {
        return roof;
    }

    public String getLights() {
        return lights;
    }

    @Override
    public String toString() {
        return "Building{" +
                "floor='" + floor + '\'' +
                ", door='" + door + '\'' +
                ", window='" + window + '\'' +
                ", roof='" + roof + '\'' +
                ", lights='" + lights + '\'' +
                '}';
    }
}
