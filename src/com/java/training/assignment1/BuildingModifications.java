package com.java.training.assignment1;

public interface BuildingModifications {

    void slideDoor();
    void heatFloor();

}
