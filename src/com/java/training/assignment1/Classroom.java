package com.java.training.assignment1;

public class Classroom extends Building implements BuildingModifications{

    public Classroom(String floor, String door, String window, String roof, String lights) {
        super(floor, door, window, roof, lights);
    }

    @Override
    public void openCloseDoor() {
        System.out.println("Opening and closing classroom door");
    }

    @Override
    public void openCloseWindow() {
        System.out.println("Opening and closing and classroom window");
    }
    public void openCloseWindow(boolean automatic){
        System.out.println("Using automatic window to open and close classroom door");
    }

    @Override
    public void switchLightOnOff() {
        System.out.println("Switching the classroom light on and off");
    }

    @Override
    public void slideDoor(){
        System.out.println("Sliding the classroom backroom door");
    }

    @Override
    public void heatFloor(){
        System.out.println("Heating classroom floor");
    }

    @Override
    public String toString() {
        return "Building[" +
                "Classroom floor='" + floor + '\'' +
                ", Classroom door='" + door + '\'' +
                ", Classroom window='" + window + '\'' +
                ", Classroom roof='" + roof + '\'' +
                ", Classroom lights='" + lights + '\'' +
                ']';
    }
}
