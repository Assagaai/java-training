package com.java.training.assignment4;

import java.util.*;

public class StudentManagement implements Comparable<StudentManagement>{

    private String fullName;
    private Character grade;
    private int age;
    Set<StudentManagement> studentList = new TreeSet<>();

    public StudentManagement(){
        studentList.add(new StudentManagement("Tebogo Assagaai",'C', 22));
        studentList.add(new StudentManagement("Steven Jacobs",'A',20));
        studentList.add(new StudentManagement("Blake Johnson",'B',19));
        studentList.add(new StudentManagement("Sibonelo Madiva",'E',23));
    }

    public StudentManagement(String fullName, char grade, int age) {
        this.fullName = fullName;
        this.grade = grade;
        this.age = age;
    }

    public String getFullName() {
        return fullName;
    }

    public char getGrade() {
        return grade;
    }

    public int getAge() {
        return age;
    }

    public void viewStudentsInOrder(){
        for (StudentManagement studentManagement : studentList) {
            System.out.println(studentManagement);
        }
        System.out.println("---");
        System.out.println("Total student count: "+studentList.size());
    }

    public void addStudent(){

        Scanner input =new Scanner(System.in);
        System.out.println("Please enter the student's full name");
        String name = input.nextLine();
        System.out.println("Please enter their grade");
        char grade = input.nextLine().charAt(0);
        System.out.println("Please enter their age:");
        int age = input.nextInt();

        studentList.add(new StudentManagement(name,grade,age));
        for (StudentManagement studentManagement : studentList) {
            System.out.println(studentManagement);
        }
        System.out.println("Student added!"+'\n'+"---");
        System.out.println("Total student count: "+studentList.size());
    }

    public void deleteStudent(){
        Scanner input = new Scanner(System.in);
        System.out.println("Please type the name of the student you want to remove");
        String name = input.nextLine();

        studentList.removeIf(sm -> sm.getFullName().equalsIgnoreCase(name));
        for(StudentManagement sl: studentList){
            System.out.println(sl);
        }
        System.out.println("Student deleted!"+'\n'+"---");
        System.out.println("Total student count: "+studentList.size());

    }

    public void findGrade(){
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter the name of the student whose grade you want to view");
        String name = input.nextLine();

        for (StudentManagement sm : studentList) {
            if (sm.getFullName().equalsIgnoreCase(name))
                System.out.println("The grade for "+sm.getFullName()+" is: "+sm.getGrade());
        }

    }

    @Override
    public String toString() {
        return  "Name: "+fullName +", Grade: "+ grade+", Age: "+age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof  StudentManagement)) return false;
        StudentManagement that = (StudentManagement) o;
        return age == that.age &&
                fullName.equals(that.fullName) &&
                grade.equals(that.grade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFullName(), getGrade(), getAge());
    }

    @Override
    public int compareTo(StudentManagement o) {
        return this.grade.compareTo(o.getGrade());
    }
}
