package com.java.training.assignment4;

import java.util.Scanner;

public class Main {


    public static void main(String... studentApplication) {

        Scanner input = new Scanner(System.in);
        StudentManagement studentManagement = new StudentManagement();

        boolean cont = true;
        System.out.println("*** Welcome to a simple Java Student Application ***");
        while (cont) {
            System.out.println();
            System.out.println("== Enter 1 to view all current students in order ==");
            System.out.println("== Enter 2 for adding a student ==");
            System.out.println("== Enter 3 for finding grades for a particular student ==");
            System.out.println("== Enter 4 to delete a student ==");
            System.out.println("== Enter 5 To exit the program ==");

            int choice = input.nextInt();
            switch (choice) {
                case 1:
                    studentManagement.viewStudentsInOrder();
                    break;

                case 2:
                    studentManagement.addStudent();
                    break;

                case 3:
                    studentManagement.findGrade();
                    break;

                case 4:
                    studentManagement.deleteStudent();
                    break;

                default:
                    cont = false;

            }

        }
    }

}
