package com.java.training.assignment2;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Scanner;

public class Assignment2 {

    private String name;
    private int birthYear;
    private double decimalNumberCheck;


    public static void main(String[] args){

        Assignment2 assignment = new Assignment2();

        //app results
        System.out.println("*** Welcome to a simple Java program ***");
        assignment.nameReverse();
        System.out.println("---");
        assignment.daysAlive();
        System.out.println("---");
        assignment.decimalDetect();
        System.out.println("---");
    }

    void nameReverse(){

        Scanner input = new Scanner(System.in);
        System.out.println("First, enter your fullname:");
        name = input.nextLine();

        char[] nameStore = name.toCharArray();
        StringBuilder reverserName = new StringBuilder();
        for(int i = nameStore.length-1; i>-1; i--){
            reverserName.append(nameStore[i]);
        }

        System.out.println("Your name in reverse is: "+reverserName);

    }
    void daysAlive(){

        Scanner input = new Scanner(System.in);
        System.out.println("Second, please enter your birth year:");

        birthYear = input.nextInt();
        int currentYear = LocalDate.now().getYear();

        System.out.println("You have been alive for: "+((currentYear - birthYear)*365)+" days");
    }
    void decimalDetect(){
        Scanner input = new Scanner(System.in);
        System.out.println("Lastly, please enter any decimal value:");
        decimalNumberCheck = input.nextDouble();

        StringBuilder number = new StringBuilder(new DecimalFormat("00.00")
                .format(decimalNumberCheck));
        String leftSide = number.substring(0,2);
        String rightSide = number.substring(3,5);

        if(leftSide.equals(rightSide)){
            System.out.println("["+leftSide+" : "+rightSide+"]");
            System.out.println("Your inserted decimal value is identical on both sides");
        }
        else{
            System.out.println("["+leftSide+" : "+rightSide+"]");
            System.out.println("Your inserted decimal value is NOT identical on both sides");
        }
    }

}
