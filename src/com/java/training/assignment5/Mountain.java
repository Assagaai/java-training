package com.java.training.assignment5;

import java.util.Arrays;
import java.util.Scanner;

public class Mountain {

    public void startProcess(){
        Scanner input = new Scanner(System.in);
        System.out.println("1. Please enter the size you wish your list to be");
        int size = input.nextInt();

        int[] list = new int[size];

        System.out.println("The size of your list will be: "+size);
        System.out.println();
        System.out.println("2. please enter the values that fit within your list size:");
        for(int i = 0; i<list.length; i++){
            list[i] = input.nextInt();
        }
        System.out.println("Your inserted values are:"+ Arrays.toString(list));
        //Sorting the array first
        Arrays.sort(list);
        System.out.println("3. The highest number in your list is: "+list[list.length-1]);
        System.out.println("4. The lowest number in your list is: "+list[0]);
        System.out.println("5. The difference between the highest and lowest number is: "+((list[list.length-1])-list[0]));

        //Finding similar values in array
        for(int i = 0; i<size; i++){
            for(int j = i+1; j<size; j++){
                if(list[i] == list[j]){
                        System.out.println("6. There also exists a table in your chart, specifically on the value: "+list[i]);
                }
            }
        }

    }
}
