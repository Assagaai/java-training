package com.java.training.assignment5;

public class Main {

    public static void main(String... main){
        Mountain mountain = new Mountain();
        System.out.println("*** Welcome to a simple Java Class");
        System.out.println("In this program you will be working with a simple list");
        System.out.println("---");
        mountain.startProcess();
        System.out.println("---");
        System.out.println("*** Thank you for trying this application out ***");
    }
}
